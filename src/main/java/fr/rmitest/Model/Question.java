package fr.rmitest.Model;

import lombok.Data;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

@Data
public class Question implements Remote {

    private Collection<Response> responses;
    private String content;
    private User user;

    public Question(User user, String content) {
        this.user = user;
        this.content = content;
    }

    public void addResponse(Response response) throws RemoteException {
        this.responses.add(response);
    }

    public void removeReponse(Response response) throws RemoteException {
        this.responses.removeIf(r -> r.equals(response));
    }
}
