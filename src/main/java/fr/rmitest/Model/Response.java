package fr.rmitest.Model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Response implements Serializable {
    private String response;

    public Response(String response) {
        this.response = response;
    }
}

