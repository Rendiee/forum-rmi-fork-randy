package fr.rmitest.Model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class User implements Remote {

    private String username;
    private String password;
    private UUID token;

    private List<Question> questions;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.token = UUID.randomUUID();
        this.questions = new ArrayList<>();
    }

    public void addQuestion(Question q) throws RemoteException {
        this.questions.add(q);
    }

    public List<Question> getQuestions() throws RemoteException {
        return this.questions;
    }
}
